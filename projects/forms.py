from django.forms import ModelForm
from projects.models import Project


class ProjectForm(ModelForm):
    # email_address = forms.EmailField(max_length=300, required=False)
    class Meta:
        model = Project
        fields = ["name", "description", "owner"]
