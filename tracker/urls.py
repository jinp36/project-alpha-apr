from django.contrib import admin
from django.urls import path, include
from .views import home_page


urlpatterns = [
    path("", home_page, name="home"),
    path("tasks/", include("tasks.urls")),
    path("accounts/", include("accounts.urls")),
    path("projects/", include("projects.urls")),
    path("admin/", admin.site.urls),
]
